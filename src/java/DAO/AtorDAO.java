/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import bean.Ator;
import bean.Filme;
import fabricaDeSessoes.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author spyker
 */
public class AtorDAO {

    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarAtor(Ator ator) {

        Session session = fabrica.openSession();
        session.beginTransaction();

        session.save(ator);

        session.getTransaction().commit();
        session.close();
    }

    public Ator retornaAtor(int id) {
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Ator where id = :ator");
        Ator ator = (Ator) q.setParameter("ator", id).uniqueResult();
        session.close();
        return ator;
    }

    public List<Ator> retornaAtores() {

        List<Ator> atores;
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Ator");
        atores = q.list();
        session.close();

        return atores;
    }

    public void atualizarAtor(int id, String nome) {

        Ator ator = this.retornaAtor(id);
        ator.setNome(nome);

        Session session = fabrica.openSession();
        session.beginTransaction();

        session.update(ator);

        session.getTransaction().commit();
        session.close();
    }

    public void removerAtor(int id) {
        Ator ator = this.retornaAtor(id);
        List<Filme> filmes = ator.getFilmes();
        for (Filme filme : ator.getFilmes()) {
            filme.getAtor().remove(ator);
        }

        Session session = fabrica.openSession();
        if (filmes.size() > 0) {
            session.beginTransaction();

            for (Filme filme : filmes) {
                session.update(filme);
            }

            session.getTransaction().commit();
        }
        session.beginTransaction();
        session.delete(ator);
        session.getTransaction().commit();

        session.close();
    }
}
