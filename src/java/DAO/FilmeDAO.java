/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import bean.Ator;
import bean.Filme;
import fabricaDeSessoes.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author spyker
 */
public class FilmeDAO {

    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarFilme(List<Integer> atoresId, String titulo) {
        
        List<Ator> atores = new ArrayList();
        AtorDAO atorDAO = new AtorDAO();
        
        for(int atorID : atoresId){
            atores.add(atorDAO.retornaAtor(atorID));
        } 
            
        Filme filme = new Filme();
        filme.setAtor(atores);
        filme.setTitulo(titulo);
        filme.setAnoLancamento(new Date());
        
        Session session = fabrica.openSession();
        session.beginTransaction();

        session.save(filme);

        session.getTransaction().commit();
        session.close();
    }
    public void atualizarFilme(int id, List<Integer> atoresId, String titulo) {
        Filme filme = this.retornaFilme(id);

        List<Ator> atores = new ArrayList();
        AtorDAO atorDAO = new AtorDAO();
        
        for(int atorID : atoresId){
            atores.add(atorDAO.retornaAtor(atorID));
        } 
        
        filme.setAtor(atores);
        filme.setTitulo(titulo);
        
        Session session = fabrica.openSession();
        session.beginTransaction();

        session.update(filme);

        session.getTransaction().commit();
        session.close();
    }
    
    public void removerFilme(int id) {
        Filme filme = this.retornaFilme(id);
        
        Session session = fabrica.openSession();
        session.beginTransaction();

        session.delete(filme);

        session.getTransaction().commit();
        session.close();
    }
    
    
    public Filme retornaFilme(int id) {
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Filme where id = :filme");
        Filme filme = (Filme) q.setParameter("filme", id).uniqueResult();
        
        session.close();
        return filme;
    }
    
    public List<Filme> retornaFilmes() {
        List<Filme> retorno;
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Filme");
        retorno = q.list();
        session.close();
        return retorno;
    }
}
