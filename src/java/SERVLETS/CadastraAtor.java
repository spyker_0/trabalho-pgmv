/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.AtorDAO;
import bean.Ator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public class CadastraAtor implements Acao {
    public String executar(HttpServletRequest request, HttpServletResponse response){
        AtorDAO atorDAO = new AtorDAO();
        Ator ator = new Ator(request.getParameter("nome"));
        
        String id = request.getParameter("id");
        if (id == null || id.isEmpty() || id.equalsIgnoreCase("0")) {
            atorDAO.adicionarAtor(ator);
        } else {
            atorDAO.atualizarAtor(Integer.parseInt(id),request.getParameter("nome"));
        }
        
        return "/Controller?acao=ListarAtor";
    }
}
