/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.FilmeDAO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public class CadastrarFilme implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        FilmeDAO filmeDAO = new FilmeDAO();
        List<Integer> atores = new ArrayList();
        try {
            String arratores[] = request.getParameterValues("atores");

            for (int i = 0; i < arratores.length; i++) {
                atores.add(Integer.parseInt(arratores[i]));
            }
        } catch (Exception e) {
        }
        String id = request.getParameter("id");
        if (id == null || id.isEmpty() || id.equalsIgnoreCase("0")) {
            filmeDAO.adicionarFilme(atores, request.getParameter("titulo"));
        } else {
            filmeDAO.atualizarFilme(Integer.parseInt(id), atores, request.getParameter("titulo"));
        }

        return "/Controller?acao=ListarFilme";
    }
}
