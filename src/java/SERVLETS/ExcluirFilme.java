/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.AtorDAO;
import DAO.FilmeDAO;
import bean.Ator;
import bean.Filme;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public class ExcluirFilme implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        FilmeDAO fDAO = new FilmeDAO();

        fDAO.removerFilme(Integer.parseInt(request.getParameter("id")));

        return "/Controller?acao=ListarFilme";
    }

}
