/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.AtorDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public class ListarAtor implements Acao {
    
     public String executar(HttpServletRequest request, HttpServletResponse response){
        AtorDAO ator = new AtorDAO();
        
        request.setAttribute("atores", ator.retornaAtores());

        return "/AtorLista.jsp";
    }

}
