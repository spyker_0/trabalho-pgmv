/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.FilmeDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public class ListarFilme implements Acao {
    public String executar(HttpServletRequest request, HttpServletResponse response){
        FilmeDAO filmeDAO = new FilmeDAO();
        
        request.setAttribute("filmes", filmeDAO.retornaFilmes());

        return "/FilmeLista.jsp";
    }
}
