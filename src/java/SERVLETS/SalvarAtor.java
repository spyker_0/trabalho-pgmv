/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.AtorDAO;
import DAO.FilmeDAO;
import bean.Ator;
import bean.Filme;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public class SalvarAtor implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        
        AtorDAO aDAO = new AtorDAO();
        String id = request.getParameter("id");

        Ator ator;
        
        if (id == null || id == "") {
            ator = new Ator();
        } else {
            ator = aDAO.retornaAtor(Integer.parseInt(id));
        }

        request.setAttribute("ator", ator);

        return "/AtorCadastro.jsp";
    }

}
