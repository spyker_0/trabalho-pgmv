/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.AtorDAO;
import DAO.FilmeDAO;
import bean.Ator;
import bean.Filme;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maicon
 */
public class SalvarFilme implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        AtorDAO aDAO = new AtorDAO();
        FilmeDAO fDAO = new FilmeDAO();

        String id = request.getParameter("id");

        Filme filme;
        List<Ator> atores = aDAO.retornaAtores();

        if (id == null || id == "") {
            filme = new Filme();
        } else {
            filme = fDAO.retornaFilme(Integer.parseInt(id));
            for (Ator at : atores) {
                for (Ator ab : filme.getAtor()) {
                    if (at.getId() == ab.getId()) {
                        at.setCheckado(true);
                    }
                }
            }
        }

        request.setAttribute("filme", filme);

        request.setAttribute("atores", atores);

        return "/FilmeCadastro.jsp";
    }

}
