/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author spyker
 */
@Entity
@Table(name = "Ator")
public class Ator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String nome;

//    @ManyToMany(targetEntity = Filme.class, fetch = FetchType.EAGER, cascade={CascadeType.PERSIST, CascadeType.MERGE})
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "atores", targetEntity = Filme.class)
    private List<Filme> filmes = new ArrayList();

    @Transient
    private boolean checkado;

    public boolean isCheckado() {
        return checkado;
    }

    public void setCheckado(boolean checkado) {
        this.checkado = checkado;
    }

    public Ator() {

    }

    public Ator(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Filme> getFilmes() {
        return this.filmes;
    }

    public void setFilmes(List<Filme> filmes) {
        this.filmes = filmes;
    }

    public void setFilme(Filme filme) {
        this.filmes.add(filme);
    }
}
