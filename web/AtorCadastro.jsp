<%-- 
    Document   : AtorCadastro
    Created on : 12/03/2017, 16:03:36
    Author     : Maicon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Atores</title>
    </head>
    <body>
        <h1>Cadastro</h1>
        <form action="Controller" method="post">
            <input type="hidden" name="acao" value="CadastraAtor"/>
            <input type="hidden" name="id" <c:out value="${ator.id}"/>
            <label>Nome:</label><input type="text" name="nome" <c:out value="${ator.nome}"/>
            <button type="submit">Cadastrar</button>
        </form>
    </body>
</html>
