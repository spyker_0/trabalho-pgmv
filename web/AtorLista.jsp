<%-- 
    Document   : AtorLista
    Created on : 12/03/2017, 16:06:25
    Author     : Maicon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Atores</title>
        <style>
            table tr{
                text-align: left!important;
                
            }
            table, table tr td, table tr th{
                border: solid 1px;
            }
        </style>
    </head>
    <body>
        <h1>Atores</h1>
        <table>
            <tr>
                <th>Id</th>                
                <th>Nome</th>
            </tr>
        <c:forEach var="ator" items="${atores}">
            <tr>
                <td>
                    <a href="Controller?acao=SalvarAtor&id=${ator.id}">${ator.id}</a>                    
                </td>
                <td>
                    ${ator.nome}
                </td>
                <td>
                    <a href="Controller?acao=ExcluirAtor&id=${ator.id}">Deletar</a>                    
                </td>
            </tr>
            
        </c:forEach>
        </table>
        <a href="index.html">Home</a>
        </body>
</html>
