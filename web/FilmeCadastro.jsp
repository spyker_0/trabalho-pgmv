<%-- 
    Document   : FilmeCadastro
    Created on : 12/03/2017, 16:04:30
    Author     : Maicon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Filmes</title>
    </head>
    <body>
        <h1>Cadastro de filmes</h1>
        <form action="Controller">
            <input type="hidden" name="acao" value="CadastrarFilme"/>
            <input type="hidden" name="id" value="<c:out value="${filme.id}" />"/>
            <h3>Atores Disponíveis</h3>
            <c:forEach var="ator" items="${atores}">
                <div><input type="checkbox" name="atores" <c:if test="${ator.checkado}">checked</c:if> value="${ator.id}"/><label>${ator.nome}</label></div>
            </c:forEach>
            <br><label>Título</label><input type="text" name="titulo" value="<c:out value="${filme.titulo}" />">
        <button type="submit">Cadastrar</button>
        </form>
    </body>
</html>
