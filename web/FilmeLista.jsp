<%-- 
    Document   : FilmeLista
    Created on : 12/03/2017, 16:05:02
    Author     : Maicon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Filmes</title>
        <style>
            table tr{
                text-align: left!important;
                
            }
            table, table tr td, table tr th{
                border: solid 1px;
            }
        </style>
    </head>
    <body>
        <h1>Filmes</h1>
        <table>
            <tr>
                <th>Id</th>                
                <th>Titulo</th>
            </tr>
        <c:forEach var="filme" items="${filmes}">
            <tr>
                <td>
                    <a href="Controller?acao=SalvarFilme&id=${filme.id}">${filme.id}</a>                    
                </td>
                <td>
                    ${filme.titulo}
                </td>
                <td>
                    <a href="Controller?acao=ExcluirFilme&id=${filme.id}">Deletar</a>                    
                </td>
            </tr>
            
        </c:forEach>
        </table>
        <a href="index.html">Home</a>
        </body>
</html>
